require 'socket'

module LIRC
  class Client
    def initialize dev = nil
      @state = "Not started"
      @lines = 0
      @line = "not used"
      puts "Initialising"
      dev ="/var/run/lirc/lircd"
      @sock = UNIXSocket.new dev
      #puts "#{@sock.methods}"
      puts "Ready"
    end
    def write(command)
      @sock.write(command+"\n")
      @sock.flush
      #puts "Written #{command}"
      #puts @sock.readline
    end
    def read
      puts "Client.read"
      @sock.readline
    end
    def getmessage
      while true
        str = @sock.readline.chomp
        #puts "read: \"" + str + "\""
        if str == "BEGIN"
          @state = "COMMAND"
          @status = "UNKNOWN STATUS"
          @datalines = []
          @command = ""
        elsif str == "END"
          @state = ""
          if @status != "SUCCESS"
            #puts "%s sending command %s" % [@status, @command]
            #if report_errors
              return @status
          else
            #do nothing, wait for next command
          end
        elsif str == "DATA"
          @state = "DATACOUNT"
        elsif @state == "COMMAND"
          @command = str
          @state = "RESULT"
        elsif @state == "RESULT"
          if str == "SUCCESS"
            @status = "SUCCESS"
          elsif str == "ERROR"
            @status = "ERROR"
          else
            @status = "UNKNOWN"
          end
          @state = "DATA_OR_END"
          @data = nil
        elsif @state == "DATACOUNT"
          @linecount = str.to_i
          @state = "DATA"
        elsif @state == "DATA"
          @datalines += [str]
          @linecount -= 1
          if @linecount == 0
            @state = "COMPLETE"
          end
        else
#          puts "Default exit"
          @state = ""
          return str
        end
#        puts @state
      end	#while true
    end
    def next
      puts "Client.next"
      Event.new @sock.getmessage
    end
    def each_event
      puts "Client.each_event"
      return Enumerator.new(self, :each_event) unless block_given?
      loop { yield self.next }
    end
    alias each each_event
  end
  class Event
    attr_reader :code, :repeat, :name, :remote
    def initialize str
      puts "Event.initialize"

      #else
        code, repeat, @name, @remote = str.split(' ', 4)
        @code = code.hex
        @repeat = repeat.hex
      #end
    end
    def repeat?
      @repeat > 0
    end
    def to_s
      puts "Event.to_s"
      if @state != ""
        "%02x %s %s" % [@repeat, @name, @remote]
      else
        puts @state
        puts @lines
        puts @line
        "%d %s %s" % [@lines, @state, @line]
      end
    end
  end
end

