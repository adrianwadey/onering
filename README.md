#OneRing
*One Ring to rule them all,*  
*One Ring to find them,*  
*One Ring to bring them all*  
*And in the darkness bind them*

A lirc based remote control package to allow control of multiple equipment from a single remote. At the moment it has hard-coded translation of one remote command to another.

Written in ruby script because I thought it would be good to learn ruby, plus rails looks like it has great web/db support that would be useful for configuration of a more full-blown/versatile version.

My Audio System doesn't have IR commands to select specific inputs, so I found a command (Play) that always puts it into a known state (actually you have to wait about 10 secs in case there is a disc in the machine) and then send AUX commands at timed intervals to get to the input you want.

##Problems
At the moment everything is executed sequentially. Pauses cause problems because they block. e.g. volume up/down repeats seem to need a pause, I guess to avoid sending when the remote control is sending, however the pause means you don't know when the next (repeated key) signal arrives to start the timing from. I just played with some values till I found something that worked.

